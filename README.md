# Analisis de *sistema de Farmacia*
## Herramientas
- [Editor Plantuml](https://sujoyu.github.io/plantuml-previewer/)
- [Extension firefox](https://addons.mozilla.org/en-US/firefox/addon/plantuml-visualizer/)
- [Ayuda en linea](https://plantuml.com/sitemap-language-specification) 
___
## Diagramas 
### Casos de uso
- [global](/diagramas/USCglobal.txt)

![alternative text](http://www.plantuml.com/plantuml/proxy?cache=no&src=https://gitlab.com/vonbeitthia/FarmaciaV1/-/blob/master/diagramas/USCglobal.txt)
```plantuml
@startuml
Bob -> Alice : hello
@enduml
```` 
